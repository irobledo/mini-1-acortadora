import webapp
import urllib.parse


formulario = """
    <form action="" method="POST">
    Introduce url a acortar: <input type="text" name="url"><br>
    Introduce el recurso: <input type="text" name="short"><br>
    <input type="Submit" value="Enviar"></form>
"""


class Shortener(webapp.webApp):
    acorta ={
    }

    def parse(self, request):
        # Extraigo la información de la petición
        recurso = request.split(' ',2)[1]
        metodo = request.split(' ',2)[0]
        cuerpo = request.split('/n')[-1]
        print(cuerpo)

        return (recurso,metodo,cuerpo)

    def process(self, parsedRequest):
        recurso, metodo, cuerpo = parsedRequest

        # Si el método es GET y el recurso es \
        if recurso == '/':
            if metodo == 'GET':
                httpCode = '200 OK'
                htmlBody = '<html><body><h4>Hola! Bienvenidos a la aplicacion acortadora</h4>' + formulario + 'La lista de URLS:' + str(self.acorta) + '</body></html>'

            if metodo == 'POST':
              # Me tengo que quedar con el cuerpo de la petición
                url = (cuerpo.split('&')[0]).split('=')[-1]
              # Arreglamos la url
                url_decod = urllib.parse.unquote(url)
                if url_decod[:4] != 'http':
                    url_decod = 'http://' + url_decod
                short = cuerpo.split('=')[-1]
                self.acorta[short] = url_decod
                httpCode = '200 OK'
                htmlBody ='<html><body><h5>Url a acortar: </h5><a href="' + url_decod + '">URL A ACORTAR<br></a>' \
                        '<h5>Recurso: </h5><br><a href="' +short + '">URL ACORTADA<br></body></html>'


        else:
            rec = recurso.split('/', 1)[-1]  # Si la peticion es GET /recurso me tengo que quedar con el recurso

            # Si el recurso esta almacenado en el diccionario, redirigimos hacia la url asociada a ese recurso
            if rec in self.acorta:
                httpCode = '200 OK'
                htmlBody = "<html><body><meta http-equiv='refresh'" \
                           "content='1 url=" + self.acorta[rec] + "'>" + \
                           "</body></html>"
            else:
                httpCode = '404 Not Found'
                htmlBody = '<html><body>ERROR! Recurso no encontrado</body></html>'

        return(httpCode,htmlBody)



if __name__ == "__main__":
    testWebApp = Shortener("localhost", 1234)
